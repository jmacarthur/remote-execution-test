#!/bin/bash
# BuildStream/BuildGrid/BuildBox Remote execution test script.

# This script is a prototype and should not be relied upon for
# anything.  It will kill processes and delete files on the local and
# remote host and leave temporary files around. Please use it at your
# own risk and understand what it's doing; if in any doubt don't use
# it.

# This runs with the INSTALLED version of bst and the local versions
# of BuildGrid (bgd) and bst-artifact-server on the remote server.
# You need passwordless access to a remote server on which you can run
# 'bst-artifact-server' and 'bgd', and the remote should also have
# buildbox installed.

set +x

# remotehost: runs the artifact cache, buildgrid server, buildgrid bot
# and buildbox. You must have passwordless ssh access to this machine.
remotehost=$REMOTEHOST

if [ -z $remotehost ]; then
    echo "You must specify a remote BGD/bot host with REMOTEHOST"
    exit 1
fi

# Server setup
# ------------
# The server - remotehost - must have a directory called ~/bs/server.
# This must contain server.key, server.crt, client.key and client.crt. These can be generated
# with openssl:
# openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=full.domain.to.remotehost.net" -out server.crt -keyout server.key
# And the same for client.crt and client.key.
# These four files must also be present in $certificatestore (defined below) on the machine you run this script on.

certificatestore=$HOME/bs/server

# Needs a directory to run in:
testdir=`mktemp -d -p ~/temp`

cd $testdir

if [ ! -d "buildstream-examples" ]; then
    git clone git@gitlab.com:BuildStream/buildstream-examples.git
fi

cd buildstream-examples
git checkout 13018b073e00f6e7bdaf90103cf6c5518b4c13e9
cd alpine-demo
cat <<EOF > project.conf
name: alpine-demo
element-path: elements

aliases:
  gnu: https://ftp.gnu.org/gnu/automake/
  alpine: https://gnome7.codethink.co.uk/tarballs/


remote-execution:
  action-cache-service:
    url: ""
  execution-service:
    url: http://EXECHOST:50051
  storage-service:
    url: https://STORAGEHOST:11001
    client-key: client.key
    client-cert: client.crt
    server-cert: server.crt
EOF

sed -i -e s/STORAGEHOST/$remotehost/ project.conf
sed -i -e s/EXECHOST/$remotehost/ project.conf

run_artifact_cache() {
    ssh $remotehost pkill -f "\"bst-artifact-server -p 11001\""
    ssh $remotehost cd \$HOME/bs/server \&\& rm -rf repo \&\& mkdir -p repo \&\& \$HOME/.local/bin/bst-artifact-server -p 11001 --server-key server.key --server-cert server.crt --client-certs client.crt --enable-push repo
}

run_buildgrid() {
    ssh $remotehost pkill -f "\"bgd server start\""
    ssh $remotehost cd \$HOME/bs/server \&\& \$HOME/.local/bin/bgd server start buildgrid-config
}

run_worker() {
    ssh $remotehost pkill -f "\"bgd bot\""
    ssh $remotehost cd \$HOME/bs/server \&\& \$HOME/.local/bin/bgd bot --client-key client.key --client-cert client.crt --server-cert server.crt --parent \"\" buildbox
}

# wipe caches
rm -rf ~/.cache/buildstream

run_artifact_cache &
artifact_cache_pid=$!
run_buildgrid &
buildgrid_pid=$!
sleep 1
run_worker &
worker_pid=$!

sleep 1

# Test the ssh process which was meant to start the various
# services. This only tests for early exit, but it's better than
# nothing.
echo "Artifact cache pid is $artifact_cache_pid"
echo "Buildgrid service PID is $buildgrid_pid"
echo "Worker PID is $worker_pid"

if [ -z $artifact_cache_pid ] || [ ! -d /proc/$artifact_cache_pid ]; then
    echo "Remote artifact service (PID $artifact_cache_pid) did not start."
    exit 1
fi

if [  -z $buildgrid_pid ] || [ ! -d /proc/$buildgrid_pid ]; then
    echo "Buildgrid service (PID $buildgrid_pid) did not start."
    exit 1
fi

if [ -z $worker_pid ] || [ ! -d /proc/$worker_pid ]; then
    echo "Buildgrid worker service (PID $worker_pid) did not start."
    exit 1
fi


cp $certificatestore/server.crt .
cp $certificatestore/client.key .
cp $certificatestore/client.crt .

echo "Running test"

bst track amhello.bst --deps=all
bst build amhello.bst | cat
